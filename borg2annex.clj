#!/usr/bin/env bb

(ns borg2annex
  (:gen-class)
  (:require [clojure.tools.logging :refer [info error]]
            [clojure.tools.cli :refer [parse-opts]]
            [cheshire.core :as json]
            [clojure.java.shell :as shell]
            [clojure.string :as str]
            [clojure.java.io :as io])
  (:import [java.nio.file Files]
           [java.nio.file.attribute FileAttribute]))

(defn json-decode
  "Decode JSON string with keyword keys."
  [s]
  (json/decode s true))

(defn throw-rte [& s]
  (let [msg (str/join s)]
    (error msg)
    (throw (RuntimeException. msg))))

(defn throw-exit
  "Exit because of an unexpected exit code from a called program."
  [args result]
  (throw-rte "Program exited with unexpected exit code: "
             (:exit result) " "
             args
             (->> result
                  ((juxt :err :out))
                  (remove str/blank?)
                  (map #(str \newline %))
                  (apply str))))

(defn exec
  "Execute shell command, optionally throwing on unexpected exit code."
  [expected-exit? & args]
  (let [str-args (mapv str args)
        result (apply shell/sh str-args)
        exit (:exit result)]
    (when (and expected-exit?
               (not (expected-exit? exit)))
      (throw-exit str-args result))
    (:out result)))

(defn exec-wrapper
  "Wrap exec with a prefix, passing the first argument as non-zero-abort iff not a string, otherwise `zero?`."
  [& prefix]
  (fn [& abort?-args]
    (if-not (-> abort?-args first string?)
      (apply exec (first abort?-args) (concat prefix (rest abort?-args)))
      (apply exec zero? (concat prefix abort?-args)))))

(def git (exec-wrapper "git"))
(def borg (exec-wrapper "borg"))
(defn borg-bypass-lock
  "Wraps borg, adding --bypass-lock if configured."
  [{:keys [bypass-lock]} & args]
  (if-not bypass-lock
    (apply borg args)
    (apply (exec-wrapper "borg" "--bypass-lock") args)))

(def cli-options
  (let [absolute-path #(-> % io/file .getAbsolutePath str)]
    [["-b" "--borg-repo BORG-REPO" "borgbackup repository to convert"
      :default (absolute-path "borg")
      :parse-fn absolute-path]
     ["-l" "--bypass-lock" "Bypass the borgbackup lock, useful for read-only file systems"]
     [nil "--same-chunker-params" "Forwarded to borgbackup, see `man borg-diff`"]
     ["-a" "--annex-repo ANNEX-REPO" "git-annex repository to create"
      :default (absolute-path "annex")
      :parse-fn absolute-path]
     [nil "--reset-resume"
      "Force reset to the last successful extraction commit and continue from there"]
     ["-r" "--archive-regex ARCHIVE-REGEX" "Archives to convert"
      :default #".*_[0-9]{8}_[0-9]{6}$"
      :parse-fn re-pattern]
     ["-t" "--time-regex TIME-REGEX" "Timestamp to extract from the archive name"
      :default #"[0-9]{8}_[0-9]{6}$"
      :parse-fn re-pattern]
     ["-s" "--bundle-threshold BUNDLE-THRESHOLD" "Add nested repositories this size (in bytes) and bigger as bundles instead of subtrees"
      :default (* 1 1024 1024)
      :parse-fn parse-long]
     ["-n" "--max-args MAX-ARGS" "Maximum amount of variable arguments to call programs with"
      :default 200
      :parse-fn parse-long]
     ["-e" "--exclude EXCLUDE" "Exclude patterns for borgbackup, see `man borg-patterns`, can be given multiple times"
      :multi true
      :default []
      :update-fn conj]
     ["-h" "--help" "Display this help"]]))

(defn delete-recursively!
  "Delete recursively *without* following symlinks."
  [file]
  (let [coerced-file (io/as-file file)]
    (.setWritable coerced-file true)
    (when (and (not (Files/isSymbolicLink (.toPath coerced-file)))
               (.isDirectory coerced-file))
      (run! delete-recursively! (.listFiles coerced-file)))
    (io/delete-file coerced-file)))

(defn borg-repo-info
  "Information about a borg repository."
  [{:keys [borg-repo] :as context}]
  (->> (borg-bypass-lock context "list" "--json" borg-repo)
       json-decode))

(defn keep-archive?
  "Keep this archive according to the context?"
  [context {:keys [archive]}]
  (re-matches (:archive-regex context) archive))

(defn remove-git-files
  "Remove .git/** files from a sequence of files, includes top-level .git."
  [paths]
  (remove #(re-matches #"(?:^|.*/)\.git(?:/.*|$)" %) paths))

(defn json-lines
  "Parse string as JSON lines."
  [s]
  (->> s
       str/split-lines
       (remove str/blank?)
       (map json-decode)))

(defn git-repo-dir
  "Remove /.git suffix, hence does NOT work on top-level .git."
  [dot-git]
  (str/replace dot-git #"/\.git$" ""))

(defn exclude-args [{:keys [exclude]}]
  (interleave (repeat "--exclude")
              exclude))

(defn git-repos
  "Get set of paths of non-bare Git repositories in a given borg archive."
  [{:keys [borg-repo] :as context} archive]
  (->> (apply borg-bypass-lock context
              (concat ["list"
                       "--json-lines"]
                      (exclude-args context)
                      [(str borg-repo "::" (:archive archive))
                       "re:.*/\\.git$"]))
       json-lines
       (filter #(-> % :type #{"d"}))
       (map :path)
       (map git-repo-dir)
       set))

(defn batch
  "Batch calls to a program / function to avoid running into limits.
   Does not call f if elements is empty. Returns sequence of results."
  [{:keys [max-args]} f & args-and-elements]
  (let [f-args (butlast args-and-elements)
        elements (last args-and-elements)]
    (->> elements
         (partition-all max-args)
         (map #(apply f (concat f-args %)))
         doall)))

(defn changes-between
  "Determine relevant changes between two borg archives.
   If first archive is omitted assumes empty archive."
  [{:keys [borg-repo same-chunker-params] :as context}
   {a-name :archive} {b-name :archive}]
  (let [changes (when a-name
                  (->> (apply borg-bypass-lock context
                              #{0
                                1 ; we have to tolerate EXIT_WARNING here because of potentially different chunker params
                                  ; https://github.com/borgbackup/borg/blob/35d76f9a641a80358a0f1960e2c5ade6135690af/src/borg/archiver.py#L1135-L1145
                                }
                              "diff"
                              (concat (when same-chunker-params ["--same-chunker-params"])
                                      (exclude-args context)
                                      [(str borg-repo "::" a-name)
                                       b-name
                                       "--json"]))
                       json-lines))
        has-type (fn [& types] #(some (set types) (->> % :changes (map :type))))
        mode-and-afterwards-dir #(some (fn [change]
                                         (and (#{"mode"} (:type change))
                                              (-> change :new_mode (str/starts-with? "d"))))
                                       (->> % :changes))
        mode-except-stays-dir #(some (fn [change]
                                       (and (#{"mode"} (:type change))
                                            (not
                                             (and
                                              (-> change :old_mode (str/starts-with? "d"))
                                              (-> change :new_mode (str/starts-with? "d"))))))
                                     (->> % :changes))
        changed-non-dotgit-files
        (->> (if a-name
               (->> changes
                    (remove (some-fn (has-type "removed" "added directory" "removed directory" "removed link" "owner")
                                     mode-and-afterwards-dir)))
               (->> (apply borg-bypass-lock context
                           (concat ["list"
                                    "--json-lines"
                                    (str borg-repo "::" b-name)]
                                   (exclude-args context)))
                    json-lines
                    (remove #(#{"d"} (:type %)))))
             (map :path)
             remove-git-files)]
    {:to-save-subtree
     (->> changes
          (filter (has-type "removed" "removed directory" "removed link"))
          (map :path)
          (filter #(re-matches #".*/\.git$" %))
          (map git-repo-dir)
          sort)
     :to-git-rm
     (->> changes
          (filter (some-fn (has-type "removed" "removed directory" "removed link")
                           mode-except-stays-dir))
          (map :path)
          remove-git-files
          sort
          reverse)
     :to-borg-extract
     (map #(str "pf:" %)
          changed-non-dotgit-files)
     :to-git-add changed-non-dotgit-files}))

(defn author-date
  "Extract / parse author date for extraction of a given archive."
  [{:keys [time-regex]} archive]
  (re-find time-regex (:archive archive)))

(defn borg-extract!
  "Extract the files matching the patterns from the archive."
  [{:keys [borg-repo bypass-lock]} {:keys [archive]} & patterns]
  (let [args (vec (concat ["borg"]
                          (when bypass-lock ["--bypass-lock"])
                          ["extract"
                           "--noxattrs"
                           (str borg-repo "::" archive)
                           "--"]
                          patterns))
        {:keys [exit out err] :as result} (apply shell/sh args)]
    ; for some reason borg complains here but works just fine
    ; https://github.com/borgbackup/borg/issues/4110
    ; ensure the exit code is 0 or 1 (EXIT_WARNING) and all it prints is that one error to catch actual errors
    (when-not (or (zero? exit)
                  (and (= exit 1)
                       (str/blank? out)
                       (->> err
                            str/split-lines
                            (every? #(re-matches #"Include pattern '.*' never matched\."
                                                 %)))))
      (throw-exit args result))))

(defn git-annex-add
  "Add files to git-annex according to context settings."
  [_context & args]
  (apply git "annex" "add" "--no-check-gitignore" "--force-large" "--" args))

(defn add-subtree!
  "Add subtree(s) for a given nested Git repository.
   Removes existing directories and creating commits as necessary.
   All refs for the repository are processed sequentially, ending with HEAD.
   The commit that HEAD points to is included even if HEAD is detached.
   Adds a bundle to the annex instead if the bundle is at least --bundle-threshold bytes."
  [{:keys [annex-repo bundle-threshold] :as context} archive git-repo]
  (info "Processing subtree" git-repo)
  (let [nested-repo (.toFile
                     (Files/createTempDirectory
                      "borg2annex-nested-"
                      (into-array FileAttribute [])))]
    (shell/with-sh-dir nested-repo
      (borg-extract! context
                     archive
                     (str "pp:" git-repo)))
    (shell/with-sh-dir (io/file nested-repo git-repo)
      (when-not (#{"HEAD\n"} (exec false "git" "rev-parse" "HEAD"))
        (let [current-ref (str/trim-newline
                           (git "rev-parse" "--symbolic-full-name" "HEAD"))
              refs (->> (git "show-ref")
                        str/split-lines
                        (remove str/blank?)
                        (map #(str/replace-first % #"^[0-9a-f]{40} " ""))
                        (apply hash-set current-ref)
                        (sort-by (juxt #{current-ref} identity)))
              bundle (.toFile
                      (Files/createTempFile
                       "borg2annex-"
                       ".bundle"
                       (into-array FileAttribute [])))
              target-file (io/file annex-repo git-repo)
              remove-target-remains #(let [file (io/file annex-repo git-repo)]
                                       (when (.exists file)
                                         (delete-recursively! file)))]
          (apply git "bundle" "create" bundle refs)
          (shell/with-sh-dir annex-repo
            (if (>= (.length bundle)
                    bundle-threshold)
              (do (info "Saving" git-repo "as bundle")
                  (shell/sh "git" "rm" "-r" git-repo)
                  (remove-target-remains)
                  (io/make-parents target-file)
                  (io/copy bundle target-file)
                  (git-annex-add context git-repo)
                  (git "commit" "-m" (str "Add as bundle: " git-repo)))
              (doseq [r refs]
                (info "Saving ref" r "of subtree" git-repo)
                (when (-> (shell/sh "git" "rm" "-r" git-repo)
                          :exit zero?)
                  (git "commit"
                       "--date" (author-date context archive)
                       "-m" (str "Delete for subtree: " r " " git-repo)))
                (remove-target-remains)
                (git "subtree" "add" "-P" git-repo (io/file nested-repo git-repo) r))))
          (io/delete-file bundle))))
    (delete-recursively! nested-repo)))

(defn commit-body [context archive]
  (str "borg2annex-repository-id: " (-> context :borg-repo-info :repository :id)
       \newline
       "borg2annex-archive-id: " (:id archive)))

(defn extract-archive!
  "Extract archive into working directory.
   Removes, updates, adds, adds subtrees to save the diff between the archives as necessary."
  [{:keys [annex-repo] :as context} previous-archive archive]
  (info "Extracting" (:archive archive))
  (shell/with-sh-dir annex-repo
    (let [{:keys [to-save-subtree to-git-rm to-borg-extract to-git-add]}
          (changes-between context previous-archive archive)]
      (info "Saving" (count to-save-subtree) "subtrees")
      (run! #(add-subtree! context previous-archive %)
            to-save-subtree)
      (run! #(let [file (io/file annex-repo %)]
               (when (.exists file)
                 (delete-recursively! file)))
            to-save-subtree)

      (info "Removing" (count to-git-rm) "files")
      (batch context git "rm" "-r" "--ignore-unmatch" "--"
             (map #(io/file annex-repo %)
                  to-git-rm))
      (run! #(let [file (io/file annex-repo %)]
               (when (.isDirectory file)
                 (io/delete-file file)))
            to-git-rm)

      (info "Extracting" (count to-borg-extract) "files")
      (batch context
             borg-extract! context archive to-borg-extract)
      (info "Committing" (:archive archive))
      (batch context
             git-annex-add context
             to-git-add)
      (git "commit"
           "--allow-empty"
           "--date" (author-date context archive)
           "-m" (str (:archive archive)
                     \newline \newline
                     (commit-body context archive))))))

(defn add-subtrees!
  "Add subtrees for all (non-top-level) Git repositories in a given archive."
  [context archive]
  (run! #(add-subtree! context archive %)
        (git-repos context archive)))

(defn resume
  "Resume conversion of sequence of archive pairs.
   Resets the target repository as necessary.
   Throws if no existing commits reachable from HEAD can be resumed from.
   Returns remaining pairs."
  [{:keys [reset-resume annex-repo] :as context} archives-pairs]
  (if reset-resume
    (shell/with-sh-dir annex-repo
      (let [extraction-commit
            #(let [commit-hash (str/trim-newline
                                (git "log" "--all-match" "--grep" (commit-body context %) "--format=%H"))]
               (when-not (str/blank? commit-hash)
                 commit-hash))
            remaining (drop-while (comp extraction-commit second)
                                  archives-pairs)]
        (if (= remaining archives-pairs)
          (throw-rte "No resumable commits found")
          (if-let [last-extracted (-> remaining ffirst
                                      (or (-> archives-pairs last second)))]
            (do (info "Resuming from archive" (:archive last-extracted))
                (git "reset" "--hard" (extraction-commit last-extracted))
                (git "clean" "-fdx")
                remaining)
            (throw-rte "Could not resume")))))
    archives-pairs))

(defn convert-archives!
  "Convert borg archives into commits in a git-annex repository."
  [context archives]
  (info "Converting" (count archives) "archives")
  (let [sorted-archives (sort-by #(author-date context %) archives)]
    (->> sorted-archives
         (concat [nil])
         (partition 2 1)
         (resume context)
         (run! #(apply extract-archive! context %)))
    (when-let [final-archive (last sorted-archives)]
      (add-subtrees! context final-archive))))

(defn create-annex!
  "Create git-annex repository."
  [{:keys [reset-resume annex-repo]}]
  (when-not reset-resume
    (info "Creating git-annex repo in" annex-repo)
    (git "init" annex-repo)
    (git "-C" annex-repo "annex" "init" annex-repo)))

(defn convert!
  "Convert borg repository into git-annex repository."
  [context]
  (create-annex! context)
  (let [repo-info (borg-repo-info context)
        context-with-repo (assoc context
                                 :borg-repo-info repo-info)]
    (->> repo-info
         :archives
         (filter #(keep-archive? context-with-repo %1))
         (convert-archives! context-with-repo))))

(defn validate-options
  "Create list of option validation errors.
   Return nil iff validation successful."
  [{:keys [borg-repo annex-repo reset-resume bypass-lock]}]
  (->> [(when-not (zero?
                   (->> ["borg" (when bypass-lock "--bypass-lock") "list" borg-repo]
                        (remove nil?)
                        (apply shell/sh)
                        (shell/with-sh-env {:BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK "yes"})
                        :exit))
          (str borg-repo " is not a valid borg repository, read about --bypass-lock if required"))
        (when-not
         (let [dir (io/file annex-repo)]
           (or reset-resume
               (not (.exists dir))
               (empty? (rest (file-seq dir)))))
          "git-annex repository to must not exist or be empty if not resuming")]
       (filter some?)
       seq))

(defn -main
  "CLI entrypoint."
  [& args]
  (let [{:keys [errors options summary]} (parse-opts args cli-options)
        validation-errors (validate-options options)
        invalid-opts-exit #(throw-rte "Invalid options: " %)]
    (cond
      (options :help) (println summary)
      errors (run! invalid-opts-exit errors)
      validation-errors (run! invalid-opts-exit validation-errors)
      :else (convert! options))))

; execute -main if run as a script
(when (= *file* (System/getProperty "babashka.file"))
  (apply -main *command-line-args*))