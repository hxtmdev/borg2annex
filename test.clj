(ns test
  #_{:clj-kondo/ignore [:refer-all]}
  (:require [clojure.test :refer :all]
            [borg2annex :refer [borg git delete-recursively! exec]]
            [clojure.java.io :as io]
            [clojure.java.shell :as shell]
            [clojure.string :as str])
  (:import [java.nio.file Files LinkOption]
           [java.nio.file.attribute FileAttribute]))

(def tmp-dir
  "Root temporary directory for testing."
  (Files/createTempDirectory
   "borg2annex-test-"
   (into-array FileAttribute [])))

(defn make-directory!
  "Create temporary directory within tmp-dir."
  []
  (str (Files/createTempDirectory
        tmp-dir
        nil
        (into-array FileAttribute []))))

(defn make-borg-repo!
  "Create a new borg repository in a temporary directory."
  []
  (doto (make-directory!)
    (#(borg "init"
            "-e" "none"
            %))))

(defn make-borg-archive!
  "Create a borg archive from a directory."
  [repo archive dir]
  (shell/with-sh-dir dir
    (borg "create" (str repo "::" archive) ".")))

(defn commits-match?
  "Do the commit subjects match the given regular expressions?"
  [regexes]
  (every? some?
          (map re-matches
               regexes
               (concat
                (str/split-lines (git "log" "--format=%s" "--reverse" "--topo-order"))
                (repeat nil)))))

(deftest delete-no-follow-symlink
  (let [src-dir (make-directory!)
        target-dir (make-directory!)
        target-file (io/file target-dir "somefile")]
    (spit target-file "test")
    (Files/createSymbolicLink (.toPath (io/file src-dir "link"))
                              (.toPath (io/file target-dir))
                              (into-array FileAttribute []))
    (delete-recursively! src-dir)
    (is (not (.exists (io/file src-dir)))
        "source deleted")
    (is (.exists target-file)
        "target not deleted")))

(deftest validate-opts
  (let [borg-repo (make-borg-repo!)
        annex-repo (make-directory!)]
    (is (-> (borg2annex/validate-options
             {:borg-repo annex-repo
              :annex-repo annex-repo})
            str/join
            (str/includes? "valid borg")))
    (is (-> (borg2annex/validate-options
             {:borg-repo borg-repo
              :annex-repo borg-repo})
            str/join
            (str/includes? "not exist or be empty")))))

(deftest empty-archive
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)]
    (make-borg-archive! borg-repo "test_20200102_030405" data)
    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)
    (is (= (shell/with-sh-dir annex-repo
             (git "log" "--format=%s"))
           "test_20200102_030405\n")
        "contains empty commit")))

(deftest first-time-regex
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)]
    (make-borg-archive! borg-repo "test_20200102_030405__20222222_222222" data)
    (shell/with-sh-env {:TZ "TEST-1"}
      (borg2annex/-main "-b" borg-repo
                        "-a" annex-repo
                        "--time-regex" "[0-9]{8}_[0-9]{6}"))
    (is (= (shell/with-sh-dir annex-repo
             (git "log" "--format=%ai"))
           "2020-01-02 03:04:05 +0100\n")
        "has first time regex match from archive name as author date")))

(deftest no-archive
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)]
    (make-borg-archive! borg-repo "ignored" data)
    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)
    (is (shell/with-sh-dir annex-repo
          (-> (shell/sh "git" "log")
              :err
              (str/includes? "does not have any commits")))
        "contains no commits")))

(deftest exclude
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)]
    (spit (io/file data "not-excluded") "foo")
    (spit (io/file data "excluded") "foo")
    (spit (io/file data "excluded2") "foo")
    (make-borg-archive! borg-repo "test_20010101_000000" data)
    (spit (io/file data "excluded") "bar")
    (spit (io/file data "excluded2") "bar")
    (make-borg-archive! borg-repo "test_20010101_010000" data)
    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo
                      "-e" "fm:excl?ded*")
    (is (= (->> annex-repo io/file .listFiles (map #(.getName %)) set)
           #{".git" "not-excluded"})
        "only .git and the non-excluded file present")))

(deftest simple-file
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)
        source-file (io/file data ".--*file123")
        converted-file (io/file annex-repo ".--*file123")]
    (spit source-file "content123")
    (make-borg-archive! borg-repo "--*test_20200102_030405" data)
    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)
    (is (= (->> annex-repo io/file .listFiles (map #(.getName %)) set)
           #{".git" ".--*file123"})
        "only .git and the file present")
    (is (= (slurp converted-file) "content123")
        "content correct")
    (is (Files/isSymbolicLink (.toPath converted-file)))
    (is (= (shell/with-sh-dir annex-repo
             (git "log" "--format=%s"))
           "--*test_20200102_030405\n")
        "contains commit subject")
    (is (re-matches (re-pattern
                     (str/join
                      ["borg2annex-repository-id: [0-9a-f]{64}\n"
                       "borg2annex-archive-id: [0-9a-f]{64}\n\n"]))
                    (shell/with-sh-dir annex-repo
                      (git "log" "--format=%b")))
        "contains borg IDs in body")))

(deftest update-files
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)]
    (spit (io/file data ".gitignore") "file\n")
    (spit (io/file data "file") "old\n")
    (spit (io/file data "file2dir") "old\n")
    (doto (io/file data "dir2file")
      .mkdirs
      (#(spit (io/file % "subfile") "old\n")))
    (.mkdirs (io/file data "empty2file"))
    (make-borg-archive! borg-repo "test_20010101_000000" data)

    (.mkdirs (io/file data "anotherempty2file"))
    (make-borg-archive! borg-repo "test_20010101_010000" data)

    (io/delete-file (io/file data "anotherempty2file"))
    (make-borg-archive! borg-repo "test_20010101_020000" data)

    (spit (io/file data "file") "new\n")
    (doto (io/file data "file2dir")
      io/delete-file
      .mkdirs
      (#(spit (io/file % "subfile") "new\n")))
    (doseq [dir ["dir2file" "empty2file"]]
      (doto (io/file data dir)
        delete-recursively!
        (#(spit % "new\n"))))
    (spit (io/file data "anotherempty2file") "new\n")

    (make-borg-archive! borg-repo "test_20010101_030000" data)

    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo
                      "--same-chunker-params")
    (let [logs (shell/with-sh-dir annex-repo
                 (git "log" "--name-status"))]
      (is (str/includes? logs "M\tfile\n") "contains regular file change")
      (is (str/includes? logs "A\tdir2file\n") "contains file addition from non-empty dir")
      (is (str/includes? logs "A\tempty2file\n") "contains file addition from empty dir")
      (is (str/includes? logs "A\tanotherempty2file\n")
          "contains file addition from empty dir not in first archive and made a file in a separate step"))
    (is (= (slurp (io/file annex-repo "file")) "new\n")
        "working directory has newest version")))

(deftest remove-file
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)]
    ; reversed order to test sorting
    (make-borg-archive! borg-repo "test_20010101_010000" data)
    (spit (io/file data "file") "content\n")
    (Files/createSymbolicLink (.toPath (io/file data "link"))
                              (.toPath (io/file data "file"))
                              (into-array FileAttribute []))
    (make-borg-archive! borg-repo "test_20010101_000000" data)
    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)
    (is (str/includes? (shell/with-sh-dir annex-repo
                         (git "log" "--name-status"))
                       "D\tfile\n")
        "contains file deletion")
    (is (str/includes? (shell/with-sh-dir annex-repo
                         (git "log" "--name-status"))
                       "D\tlink\n")
        "contains link deletion")))

(deftest readonly-directory
  (doseq [first-commit [true false]]
    (let [data (make-directory!)
          borg-repo (make-borg-repo!)
          annex-repo (make-directory!)
          ro-dir (io/file data "subdir")
          test-file (io/file ro-dir "file")]
      (when-not first-commit
        (make-borg-archive! borg-repo "test_20010101_000000" data))

      (io/make-parents test-file)
      (spit test-file "x")
      (.setWritable ro-dir false false)
      (make-borg-archive! borg-repo "test_20010101_010000" data)

      (.setWritable ro-dir true true)
      (io/delete-file test-file)
      (make-borg-archive! borg-repo "test_20010101_020000" data)

      (borg2annex/-main "-b" borg-repo
                        "-a" annex-repo)

      (is (str/includes? (shell/with-sh-dir annex-repo
                           (git "log" "--name-status"))
                         "D\tsubdir/file\n")
          "contains file deletion"))))

(deftest add-file
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)]
    (spit (io/file data "file") "content\n")
    (make-borg-archive! borg-repo "test_20010101_000000" data)
    (spit (io/file data "other") "content\n")
    (make-borg-archive! borg-repo "test_20010101_010000" data)
    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)
    (is (str/includes? (shell/with-sh-dir annex-repo
                         (git "log" "--name-status"))
                       "A\tother\n")
        "contains addition")))

(deftest update-root-mode
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)]
    (spit (io/file data "file") "x")
    (.setWritable (io/file data) true true)
    (make-borg-archive! borg-repo "test_20010101_000000" data)

    (.setWritable (io/file data) false true)
    (make-borg-archive! borg-repo "test_20010101_010000" data)

    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)

    (is (= (slurp (io/file annex-repo "file")) "x")
        "file content is correct")))

(deftest resume
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)]
    ; to test removal of ignored garbage
    (spit (io/file data ".gitignore") "ignored-garbage\n")

    (spit (io/file data "file") "old\n")
    (make-borg-archive! borg-repo "test_20010101_000000" data)

    (spit (io/file data "file") "new\n")
    (make-borg-archive! borg-repo "test_20010101_010000" data)

    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)

    ; create crashed state by resetting and creating garbage file
    (shell/with-sh-dir annex-repo
      (git "reset" "--hard" "HEAD~"))
    (spit (io/file annex-repo "garbage") "garbage")
    (spit (io/file annex-repo "ignored-garbage") "ignored-garbage")

    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo
                      "--reset-resume")

    (is (str/includes? (shell/with-sh-dir annex-repo
                         (git "log" "--name-status"))
                       "M\tfile\n")
        "contains change")
    (is (not (.exists (io/file data "garbage")))
        "garbage got removed")
    (is (not (.exists (io/file data "ignored-garbage")))
        "ignored garbage got removed")))

(deftest bypass-lock
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)
        sub-repo (io/file data "somedir")]
    (git "init" "--initial-branch=fred" sub-repo)
    (spit (io/file sub-repo "file") "x")
    (shell/with-sh-dir sub-repo
      (git "add" "file")
      (git "commit" "-m" "subtree commit"))

    (spit (io/file data "file") "x")
    (make-borg-archive! borg-repo "test_20010101_000000" data)
    (make-borg-archive! borg-repo "test_20010101_010000" data)
    (.setWritable (io/file borg-repo)
                  false)

    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo
                      "-l")

    (is (= (slurp (io/file annex-repo "file")) "x")
        "content correct")))

(deftest ignore-git
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)]
    (git "init" "--initial-branch=fred" data)
    (spit (io/file data "toplevel") "x")

    (git "init" "--initial-branch=fred" (str (io/file data "somedir")))
    (spit (io/file data "somedir" "nested") "x")

    (make-borg-archive! borg-repo "test_20010101_000000" data)
    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)

    (let [changes (shell/with-sh-dir annex-repo
                    (git "log" "--name-status"))]
      (is (str/includes? changes "A\ttoplevel\n")
          "contains toplevel workdir file")
      (is (str/includes? changes "A\tsomedir/nested\n")
          "contains nested workdir file")
      (is (not (.exists (io/file annex-repo "somedir" ".git")))
          "does not contain nested .git directory"))))

(deftest exact-dotgit
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)
        sub-repo (io/file data "somedir")
        nested-file (io/file sub-repo "file")]
    (git "init" "--initial-branch=fred" (str (io/file data "somedir")))
    (spit nested-file "x")
    (shell/with-sh-dir sub-repo
      (git "add" "file")
      (git "commit" "-m" "subtree commit"))
    (.renameTo (io/file sub-repo ".git")
               (io/file sub-repo "_git"))

    (make-borg-archive! borg-repo "test_20010101_000000" data)
    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)

    (shell/with-sh-dir annex-repo
      (is (commits-match? [#"test_20010101_000000"])))))

(deftest nondir-dotgit
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)
        dotgit (io/file data "nested" ".git")]
    (io/make-parents dotgit)
    (spit dotgit "x")

    (make-borg-archive! borg-repo "test_20010101_000000" data)
    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)

    (shell/with-sh-dir annex-repo
      (is (commits-match? [#"test_20010101_000000"])))))

(deftest git-subtree-end
  (doseq [detach [false true]]
    (let [data (make-directory!)
          borg-repo (make-borg-repo!)
          annex-repo (make-directory!)
          sub-repo (io/file data "somedir")]
      (git "init" "--initial-branch=fred" sub-repo)
      (spit (io/file sub-repo "file") "x")
      (shell/with-sh-dir sub-repo
        (git "add" "file")
        (git "commit" "-m" "subtree commit")
        (when detach
          (git "checkout" "--detach")))

      (make-borg-archive! borg-repo "test_20010101_000000" data)
      (borg2annex/-main "-b" borg-repo
                        "-a" annex-repo)

      (shell/with-sh-dir annex-repo
        (is (commits-match? (concat
                             [#"test_20010101_000000"
                              #"Delete for subtree: refs/heads/fred somedir"
                              #"subtree commit"
                              #"Add 'somedir/' from commit .*"]
                             (when detach
                               [#"Delete for subtree: HEAD somedir"
                                #"Add 'somedir/' from commit .*"])))))
      (is (= (slurp (io/file annex-repo "somedir" "file")) "x")
          "subtree content correct"))))

(deftest git-subtree-empty-dir
  (doseq [prefix-empty-archive [true false]
          bundle [true false]]
    (let [data (make-directory!)
          borg-repo (make-borg-repo!)
          annex-repo (make-directory!)
          sub-repo (io/file data "somedir")
          sub-file (io/file sub-repo "file")]
      (when prefix-empty-archive
        (make-borg-archive! borg-repo "test_20010101_000000" data))

      (git "init" "--initial-branch=fred" sub-repo)
      (spit sub-file "x")
      (shell/with-sh-dir sub-repo
        (git "add" "file")
        (git "commit" "-m" "subtree commit")
        (git "rm" "file"))
      (.mkdir sub-file) ; conflicting dir

      (make-borg-archive! borg-repo "test_20010101_010000" data)
      (apply borg2annex/-main
             "-b" borg-repo
             "-a" annex-repo
             (when bundle
               ["--bundle-threshold" "0"]))

      (shell/with-sh-dir annex-repo
        (is (commits-match? (concat
                             (when prefix-empty-archive
                               [#"test_20010101_000000"])
                             [#"test_20010101_010000"]
                             (if bundle
                               [#"Add as bundle: somedir"]
                               [#"subtree commit"
                                #"Add 'somedir/' from commit .*"]))))))))

(deftest git-bundle
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)
        sub-repo (io/file data "intermediate" "somedir")]
    (git "init" "--initial-branch=fred" sub-repo)
    (spit (io/file sub-repo "file") "x")
    (shell/with-sh-dir sub-repo
      (git "add" "file")
      (git "commit" "-m" "nested commit"))

    (make-borg-archive! borg-repo "test_20010101_000000" data)
    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo
                      "--bundle-threshold" "0")

    (shell/with-sh-dir annex-repo
      (is (commits-match? [#"test_20010101_000000"
                           #"Add as bundle: intermediate/somedir"]))
      (is (->> (io/file annex-repo "intermediate" "somedir")
               .toPath
               Files/isSymbolicLink)))))

(deftest git-subtree-no-commits
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)
        sub-repo (io/file data "somedir")]
    (git "init" sub-repo)
    (make-borg-archive! borg-repo "test_20010101_000000" data)

    (delete-recursively! sub-repo)
    (make-borg-archive! borg-repo "test_20010101_010000" data)

    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)))

(deftest git-subtree-refs
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)
        sub-repo (io/file data "somedir")]
    (git "init" "--initial-branch=hugo" sub-repo)
    (shell/with-sh-dir sub-repo
      (spit (io/file sub-repo "file") "x\n")
      (git "add" "file")
      (git "commit" "-m" "subtree commit 1")

      (spit (io/file sub-repo "file") "y\n")
      (git "add" "file")
      (git "commit" "-m" "subtree commit 2a")

      (git "checkout" "-b" "fred" "HEAD~")
      (spit (io/file sub-repo "file") "z\n")
      (git "add" "file")
      (git "commit" "-m" "subtree commit 2b")

      (git "tag" "fredfred"))

    (make-borg-archive! borg-repo "test_20010101_000000" data)
    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)

    (shell/with-sh-dir annex-repo
      (is (commits-match? [#"test_20010101_000000"
                           #"Delete for subtree: refs/heads/hugo somedir"
                           #"subtree commit 1"
                           #"subtree commit 2a"
                           #"Add 'somedir/' from commit .*"
                           #"Delete for subtree: refs/tags/fredfred somedir"
                           #"subtree commit 2b"
                           #"Add 'somedir/' from commit .*"
                           #"Delete for subtree: refs/heads/fred somedir"
                           #"Add 'somedir/' from commit .*"])))

    (let [changes (shell/with-sh-dir annex-repo
                    (git "log" "--patch" "--all"))]
      (is (str/includes? changes "-x\n+y\n")
          "contains first variant")
      (is (str/includes? changes "-x\n+z\n")
          "contains second variant"))))

(deftest git-subtree-remove
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)
        sub-repos (map #(io/file data (str "sub" %))
                       (range 2))]
    (git "init" "--initial-branch=fred" (io/file data "without-commits-ignored"))

    (doseq [sub-repo sub-repos]
      (git "init" "--initial-branch=fred" sub-repo)
      (spit (io/file sub-repo "file") "x")
      (shell/with-sh-dir sub-repo
        (git "add" "file")
        (git "commit" "-m" (str "subtree commit " (.getName sub-repo)))))

    (make-borg-archive! borg-repo "test_20010101_000000" data)

    (run! delete-recursively! sub-repos)
    (make-borg-archive! borg-repo "test_20010101_010000" data)

    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)

    (shell/with-sh-dir annex-repo
      (is (commits-match? [#"test_20010101_000000"
                           #"Delete for subtree: refs/heads/fred sub0"
                           #"subtree commit sub0"
                           #"Add 'sub0/' from commit .*"
                           #"Delete for subtree: refs/heads/fred sub1"
                           #"subtree commit sub1"
                           #"Add 'sub1/' from commit .*"
                           #"test_20010101_010000"])))
    (doseq [sub-repo sub-repos]
      (is (not (.exists sub-repo))
          "nested .git directory removed"))))

(deftest git-subtree-toplevel
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)]
    (git "init" "--initial-branch=fred" data)
    (spit (io/file data "file") "x")
    (shell/with-sh-dir data
      (git "add" "file")
      (git "commit" "-m" "subtree commit"))
    (make-borg-archive! borg-repo "test_20010101_000000" data)

    (delete-recursively! (io/file data ".git"))
    (make-borg-archive! borg-repo "test_20010101_010000" data)

    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)

    (is (= (shell/with-sh-dir annex-repo
             (str/split-lines (git "log" "--format=%s" "--reverse" "--topo-order")))
           ["test_20010101_000000"
            "test_20010101_010000"]))
    (is (= (slurp (io/file annex-repo "file")) "x")
        "working directory content correct")))

(deftest merge-repositories
  (let [converted-repo
        #(let [data (make-directory!)
               borg-repo (make-borg-repo!)
               annex-repo (make-directory!)]
           (spit (io/file data "file") %)
           (make-borg-archive! borg-repo (str % "_20010101_000000") data)
           (borg2annex/-main "-b" borg-repo
                             "-a" annex-repo)
           annex-repo)
        dst (converted-repo "dst")
        src (converted-repo "src")]
    (exec zero? "util/add-in-subdirectory" dst src "sub")
    (is (= (slurp (io/file dst "file")) "dst")
        "destination repository content correct")
    (is (= (slurp (io/file dst "sub" "file")) "src")
        "destination repository content correct")
    (is (->> (git "-C" dst "show" "git-annex:uuid.log")
             str/split-lines
             count
             (= 1))
        "remote not added to destination annex repository")))

(deftest unannex-empty-files
  (let [data (make-directory!)
        borg-repo (make-borg-repo!)
        annex-repo (make-directory!)

        empty-source (io/file data "empty")
        empty-converted (io/file annex-repo "empty")

        nonempty-source (io/file data "nonempty")
        nonempty-converted (io/file annex-repo "nonempty")

        script (-> "util/unannex-empty-files" io/file .getAbsoluteFile)]
    (spit empty-source "")
    (spit nonempty-source "x")

    (make-borg-archive! borg-repo "test_20010101_000000" data)

    (borg2annex/-main "-b" borg-repo
                      "-a" annex-repo)

    (shell/with-sh-dir annex-repo
      (exec #{1} script)
      (is (Files/isSymbolicLink (.toPath empty-converted)))
      (is (Files/isSymbolicLink (.toPath nonempty-converted)))
      (shell/with-sh-dir annex-repo
        (is (commits-match? [#"test_20010101_000000"])))

      (exec zero? script "--yes")
      (is (Files/isRegularFile (.toPath empty-converted) (into-array LinkOption [])))
      (is (Files/isSymbolicLink (.toPath nonempty-converted)))
      (shell/with-sh-dir annex-repo
        (is (commits-match? [#"test_20010101_000000"
                             #"Unannex empty files"]))))))

(let [summary (if-let [test (first *command-line-args*)]
                (run-test-var (find-var (symbol (str *ns*) test)))
                (run-tests))]
  (if (successful? summary)
    (delete-recursively! (.toFile tmp-dir))
    (System/exit 1)))