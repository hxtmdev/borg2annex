# borg2annex

Converts your borgbackup archives into a git-annex repository.

## Example

See [doc/demo.sh](doc/demo.sh)

```
##########################
# example data directory #
##########################

mydata:
nested-repo
regular-file

mydata/nested-repo:
.git
foo

* commit on main (HEAD -> main)
| * commit on test branch (testbranch)
|/
* nested commit (tag: fred)

##############################################################################
# /path/to/borg2annex/borg2annex.clj -b some-borg-repo -a new-git-annex-repo #
##############################################################################

Creating git-annex repo in /path/to/new-git-annex-repo
Converting 2 archives
Extracting mydata_20001111_111111
Saving 0 subtrees
Removing 0 files
Extracting 2 files
Committing mydata_20001111_111111
Extracting mydata_20002222_222222
Saving 0 subtrees
Removing 0 files
Extracting 1 files
Committing mydata_20002222_222222
Processing subtree nested-repo
Saving ref refs/heads/testbranch of subtree nested-repo
Saving ref refs/tags/fred of subtree nested-repo
Saving ref refs/heads/main of subtree nested-repo

##################################
# resulting git-annex repository #
##################################

new-git-annex-repo:
.git
nested-repo
regular-file

new-git-annex-repo/nested-repo:
foo

*   Add 'nested-repo/' from commit '522563918741ffb2056f55c8c3581fd24df630d5'
|\
| * commit on main
* | Delete for subtree: refs/heads/main nested-repo
* | Add 'nested-repo/' from commit 'deedd9684de2bc73e1a8c66dea0d357636a3a46f'
|\|
* | Delete for subtree: refs/tags/fred nested-repo
* |   Add 'nested-repo/' from commit '489bfa2ef76e5aa51430ad5ca051538c8805f8a7'
|\ \
| * | commit on test branch
| |/
| * nested commit
* Delete for subtree: refs/heads/testbranch nested-repo
* mydata_20002222_222222
* mydata_20001111_111111
```

## Quickstart

[Babashka](https://babashka.org/), Git, git-annex and borgbackup are required.
For important data, it is recommended to run `bb test.clj` to check compatibility with the installed versions.

Clone the repository and run `/path/to/borg2annex/borg2annex.clj -b borg-repo -a annex-repo`. Append `--help` to see all options.

Alternatively you can download and execute the program with

```sh
bb <(curl https://gitlab.com/hxtmdev/borg2annex/-/raw/main/borg2annex.clj) -b borg-repo -a annex-repo
```

Make sure to read [the code](./borg2annex.clj) first!

A more elaborate invocation could look like this

```sh
bb <(curl https://gitlab.com/hxtmdev/borg2annex/-/raw/main/borg2annex.clj) \
  -b /path/to/borg-repo \
  -a /path/to/new/annex-repo \
  --bypass-lock \
  --same-chunker-params \
  --reset-resume \
  -r 'my-archive-name_(optional-part_)?[0-9]{8}_[0-9]{6}$' \
  -e fm:'*/venv' \
  -e fm:'oldstuff/weird_encodings/filen?me.txt' \
  -e pp:'ignore/completely/'
```

## What's this?

This script converts a sequence of [borgbackup](https://borgbackup.readthedocs.io/) archives into a [git-annex](https://git-annex.branchable.com/) repository, storing nested Git repositories as [subtrees](https://git.kernel.org/pub/scm/git/git.git/plain/contrib/subtree/git-subtree.txt) or [bundles](https://git-scm.com/docs/git-bundle).
This can be useful if you took frequent snapshots of a directory with borgbackup and want to use this history from git-annex.
The two tools are not solving the same problems but sometimes use cases shift and new ones arise.
Keeping historical data around might be helpful in some cases.
See [what git-annex is not](https://git-annex.branchable.com/not/) in the git-annex documentation.

The selected borgbackup archives are replayed one by one and the changes are recorded with Git and git-annex.

This is an independent project and not affiliated with borgbackup, Git or git-annex.

## Details

### Resuming interrupted conversions

To resume an interrupted conversion simply append `--reset-resume`.
This will remove all files and commits it can't use, so don't use it after making manual changes but only for actually resuming an interrupted conversion!

### Nested Git repositories

While replaying the archive sequence, the working directory of nested Git repositories is added.
When a repository is removed from one archive to the next, all refs of the removed repository are recorded as subtrees and then removed again.

The same subtree saving procedure takes place after restoring the final archive, ending with the current HEAD of the nested repository so the git-annex repository is ready to be used with the correct state of the subtree.

Bigger repositories are added to the annex as bundles in order to keep the Git repository size down, see `--bundle-threshold`.

### .gitignore

The conversion includes files that would normally be ignored by `.gitignore`.

### git (annex) add

With git-annex, files can either be added to the Git repository or to the annex, see [git-annex-add](https://git-annex.branchable.com/git-annex-add/).

The conversion adds all files to the annex, except subtrees when attaching them in the subtree commits. This results in files being tracked twice but should not be a problem considering you'll probably not want to keep giant git repositories in your final conversion anyway.

### Commit times

The conversion uses the timestamp extracted from the archive name as the author date for commits. If the timestamp is in local time it is expected to be in the timezone of the user.

### Performance considerations

The biggest potential slowdown occurs with old borgbackup archives when borg cannot determine whether two archives use the same chunker parameters.
In this case the `borg diff` might take a long time, slowing down the conversion.

If you know the parameters are the same, you can speed up the process with [`--same-chunker-params`](https://borgbackup.readthedocs.io/en/stable/usage/diff.html).

### Final repository size

You might be able to shrink the `.git/objects/` directory by a factor of >20 by running `git gc --aggressive` after the conversion if desired.

### Merging multiple repositories

In some situations you might want to add the resulting git-annex repository of one set of archives as a subdirectory inside another git-annex repository.

Try the included utility script as a starting point

```sh
util/add-in-subdirectory DESTINATION-REPO SOURCE-REPO SUBDIRECTORY
```

The script keeps the destination repository clean of temporary metadata such as references to the source repository but does currently leave a mess in the source repository.
Feel free to submit a merge request to rectify that if that's important for your workflow.

## Limitations

### Incompatible archive timestamps

If the timestamps extracted from the archive names can not be passed to Git's `--date` option directly or you want to use the timestamps from the borgbackup archive metadata you need to modify the code.
Consider submitting a merge request if you have a clean solution that adds fairly general functionality in that direction.

### Nested Git repositories

Nesting an actual Git repository with it's on-disk format inside another is neither easy nor usually advisable.
Be aware of general Git restrictions such as `.git` receiving special handling / not being recorded.
While there is no perfect one-to-one mapping from a sequence of backups to a Git tree, this project aims to provide a useful conversion.

Everything stored inside of `.git` such as configuration, remotes, and hooks is lost in the conversion! This especially includes objects in the nested repository that were recorded in a borgbackup archive inside of `.git` but are not reachable from any ref in the last archive that includes the nested repository, such as deleted unmerged branches.

If `.git` is not a directory, the subtree is not added. If you use [worktrees](https://git-scm.com/docs/git-worktree), make sure to have the main repository in the backup as well!

Make sure you understand the consequences of saving things inside a Git / git-annex repository!

[Here](https://git-annex.branchable.com/tips/using_nested_git_repositories/)'s a discussion of nested Git repositories inside git-annex.

### Non UTF-8 characters in filenames

Invalid UTF-8 characters, such as latin1 ones, will in some cases produce `?` within borgbackup, usually throw off the string functions and usually result in exceptions because the binary arguments to some called program is not correct in some way or another.

If you only have a few problematic files you can simply exclude them with `--exclude fm:path/to/prob?em` for a file that has a problematic character between `b` and `e`. See [the borg patterns documentation](https://borgbackup.readthedocs.io/en/stable/usage/help.html#borg-help-patterns) for details.

If you have lots of those files and need automated handling, open an issue to collaborate with others who have the problem on a solution or submit a merge request directly if you have a fairly general solution that does not complicate the code too much.

If you don't have too many funny filenames, use `borg list --bypass-lock /path/to/borg-repo::first_archive | grep -F '?'` to get a good initial set of patterns.
Use `--reset-resume` if you had to add some later on to continue from the last successful archive.

### Top-level Git repositories

`.git` at the root directory of archives is ignored completely. Consider submitting a merge request with the necessary special handling and tests if you need the functionality.

### Untested scenarios

There are certain things that may or may not work but have simply not been tested and things that will likely not work without modifications, be extremely careful and verify everything works as you expect, especially in those cases:

- **bare Git repositories**: Will probably become regular annexed directories.
- **non-vanilla Git repositories**: If you already use git-annex or [Git LFS](https://git-lfs.com/) inside the backed up data things might break and/or behave in weird ways. Especially everything inside `.git` is lost in the conversion so you'll lose all extra files (for example by git-annex) stored there.
- **nested Git repositories**: If you have nested Git repositories and/or submodules, they will probably result in errors. Consider ignoring them and converting manually, opening an issue, or submitting a merge request.
- **anything exotic Git-wise**: If you use any fancy Git features double-check the result is what you expect!

## Contributing

### Bugs

Browse and report bugs here: [gitlab.com/hxtmdev/borg2annex/-/issues](https://gitlab.com/hxtmdev/borg2annex/-/issues)

### Small changes

If you have a small improvement or bugfix that does not add much complexity just open a merge request here: [gitlab.com/hxtmdev/borg2annex/-/merge_requests](https://gitlab.com/hxtmdev/borg2annex/-/merge_requests)

### Big changes

If you intend to make bigger changes or spend lots of time on something _and want it integrated into this repository_, please open an issue first to discuss it.
Of course, you have the option to maintain your own fork in line with the [license](./LICENSE).

### Commit requirements

All commits require a [DCO](https://developercertificate.org/) sign-off, which can be generated automatically by [calling `git commit` with `--signoff`](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---signoff).

Please include a body in every commit message with any details you think can help others understand what you did and why.

### Local development

For local development, use Babashka and the Clojure tooling of your choice for interactive development.
