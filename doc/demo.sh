#!/bin/bash

set -eu -o pipefail

REPO=$(cd $(dirname "${BASH_SOURCE[0]}")/.. && pwd)
cd $(mktemp -d)

# make output deterministic
export GIT_AUTHOR_NAME=borg2backup
export GIT_COMMITTER_NAME=borg2backup
export GIT_AUTHOR_EMAIL=borg2backup@example.com
export GIT_COMMITTER_EMAIL=borg2backup@example.com
export GIT_AUTHOR_DATE=19700101_000000Z
export GIT_COMMITTER_DATE=19700101_000000Z

mute () {
	"$@" &>/dev/null
}

title () {
	local text="# $1 #"
	local edge=$(<<<"$text" sed s/./#/g)
	echo "$edge"
	echo "$text"
	echo "$edge"
	echo
}

mkdir mydata

echo some content > mydata/regular-file

mute git init mydata/nested-repo --initial-branch main
cd mydata/nested-repo
echo bar > foo
mute git add foo
mute git commit -m "nested commit"
mute git tag fred
mute git checkout -b testbranch
echo baz > foo
mute git commit -am "commit on test branch"
mute git checkout main
echo barbaz > foo
mute git commit -am "commit on main"
title "example data directory"
mute cd -

ls --color=auto -AS mydata mydata/nested-repo
echo
git -C mydata/nested-repo --no-pager log --format=%s%d --graph --all | sed 's/ *$//'

borg init -e none some-borg-repo
cd mydata
borg create ../some-borg-repo::mydata_20001111_111111 .

echo some other content > regular-file
borg create ../some-borg-repo::mydata_20002222_222222 .
mute cd -
echo

args=(-b some-borg-repo -a new-git-annex-repo)
title "/path/to/borg2annex/borg2annex.clj ${args[*]}"

"$REPO"/borg2annex.clj "${args[@]}" | sed 's/.* - //;s#\(Creating git-annex repo in \).*#\1/path/to/new-git-annex-repo#'

echo
title "resulting git-annex repository"

ls --color=auto -AS new-git-annex-repo new-git-annex-repo/nested-repo
echo
git -C new-git-annex-repo --no-pager log --format=%s --graph | sed 's/ *$//'

if [[ -t 1 ]]
then
	echo
	echo Explore this demo at "$PWD"
fi
